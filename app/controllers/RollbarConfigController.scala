package controllers

import config.RollbarConfig
import org.webjars.play.WebJarsUtil
import play.api.mvc.{BaseController, ControllerComponents}

final class RollbarConfigController(
  val controllerComponents: ControllerComponents,
  maybeRollbarConfig: Option[RollbarConfig],
)(implicit webJarsUtil: WebJarsUtil)
    extends BaseController {

  lazy val show = Action(implicit request =>
    Ok(views.js.rollbarConfig.show(maybeRollbarConfig)),
  )

}
