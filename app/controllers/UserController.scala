package controllers

import actions.{UserAction, WithAdmin}
import cats.data.OptionT
import com.mohiva.play.silhouette.api.Silhouette
import com.mohiva.play.silhouette.api.util.PasswordHasherRegistry
import data.db.{AuthTokenDBIO, UserDBIO, UserDataDBIO}
import models.{User, UserData}
import mouse.boolean._
import play.api.data.Forms._
import play.api.data.{Form, FormError, Mapping}
import play.api.http.HttpErrorHandler
import play.api.i18n.{I18nSupport, Messages}
import play.api.mvc._
import silhouetteIntegration.DefaultEnv
import slick.jdbc.JdbcBackend
import utils.{FrontComponents, MainViewAuthenticatedContext, StatusType}

import scala.concurrent.Future

// TODO: DRY Users Controller with tylip

import utils.MainViewAuthenticatedContext._

final class UserController(
  val controllerComponents: ControllerComponents,
  db: JdbcBackend#DatabaseDef,
  silhouette: Silhouette[DefaultEnv],
  passwordHasherRegistry: PasswordHasherRegistry,
)(implicit
  frontComponents: FrontComponents,
  httpErrorHandler: HttpErrorHandler,
) extends BaseController
    with I18nSupport {

  implicit lazy val executionContext = controllerComponents.executionContext

  val emailField = "email" -> email

  val profileForm = Form(
    tuple(
      emailField,
      "isAdmin" -> boolean,
    ),
  )

  def filledProfileForm(user: User): Form[(String, Boolean)] =
    profileForm.fill((user.data.email, user.data.isAdmin))

  val passwordPart: (String, Mapping[String]) =
    "password" -> tuple(
      "main"    -> text(minLength = 6),
      "confirm" -> text,
    ).verifying(
      // Add an additional constraint: both passwords must match
      "users.password.dontMatch",
      ps => ps._1 == ps._2,
    )
      .transform[String](ps => ps._1, p => (p, p))
  val passwordForm = Form(single(passwordPart))

  lazy val createForm =
    Form[User](
      mapping(
        emailField,
        "isAdmin" -> boolean,
        passwordPart,
      )((email, isAdmin, password) =>
        User(
          UserData.withPassword(passwordHasherRegistry.current)(
            email,
            isAdmin,
            password,
            false,
          ),
        ),
      )((u: User) => Some((u.data.email, u.data.isAdmin, ""))),
    )

  private def generateResponse(
    user: User,
    id: User.Id,
    responseStatusWrapper: Status,
    mainForm: Form[(String, Boolean)],
    passwordForm: Form[String],
  )(implicit
    ctx: MainViewAuthenticatedContext,
    header: RequestHeader,
  ): Future[Result] =
    OptionT(db.run(AuthTokenDBIO.findByUserId(user.data.id)))
      .fold(
        BadRequest(
          views.html.users
            .edit(id, mainForm, passwordForm, None, None),
        ),
      )(authToken =>
        user.data.activated.fold(
          {
            val passwordResetUrl =
              controllers.auth.routes.ResetPasswordController
                .view(authToken.id)
                .absoluteURL()
            responseStatusWrapper(
              views.html.users.edit(
                id,
                mainForm,
                passwordForm,
                None,
                Some(passwordResetUrl),
              ),
            )
          }, {
            val activateUrl = controllers.auth.routes.GuestController
              .view(authToken.id)
              .absoluteURL()
            responseStatusWrapper(
              views.html.users.edit(
                id,
                mainForm,
                passwordForm,
                Some(activateUrl),
                None,
              ),
            )
          },
        ),
      )

  // Actions ///////////////////////////////////////////////////////////////////

  def index =
    silhouette
      .SecuredAction(WithAdmin)
      .async(implicit c =>
        for {
          users <- db.run(UserDataDBIO.all)
        } yield Ok(views.html.users.index(users)),
      )

  def fresh =
    silhouette.SecuredAction(WithAdmin)(implicit c =>
      Ok(views.html.users.fresh(createForm)),
    )

  def save =
    silhouette
      .SecuredAction(WithAdmin)
      .async(implicit request =>
        createForm
          .bindFromRequest()
          .fold(
            formWithErrors =>
              Future successful BadRequest(
                views.html.users.fresh(formWithErrors),
              ),
            user =>
              for {
                existing <- db run UserDataDBIO.byEmail(user.data.email)
                result <- existing.headOption.fold(
                  db run UserDBIO.save(user) map
                    (_ => Redirect(routes.UserController.index())),
                ) { _ =>
                  val formWithErrors = createForm
                    .fill(user)
                    .withError(
                      emailField._1,
                      Messages("users.email.alreadyInUse"),
                    )
                  Future successful BadRequest(
                    views.html.users.fresh(formWithErrors),
                  )
                }
              } yield result,
          ),
      )

  def edit(id: User.Id) =
    silhouette
      .SecuredAction(WithAdmin)
      .andThen(UserAction(db, id))
      .async(implicit request =>
        generateResponse(
          request.value,
          id,
          Ok,
          filledProfileForm(request.value),
          passwordForm,
        ),
      )

  def update(id: User.Id) =
    silhouette
      .SecuredAction(WithAdmin)
      .andThen(UserAction(db, id))
      .async(implicit request =>
        profileForm
          .bindFromRequest()
          .fold(
            formWithErrors =>
              generateResponse(
                request.value,
                id,
                BadRequest,
                formWithErrors,
                passwordForm,
              ),
            profile =>
              for {
                notAlone <-
                  db run UserDataDBIO
                    .existActiveAdminsExcept(id)
                isAdmin = profile._2
                result <-
                  if (notAlone || isAdmin) {
                    val updated = request.value.copy(
                      data = request.value.data.copy(
                        email = profile._1,
                        isAdmin = isAdmin,
                      ),
                    )
                    db run UserDBIO.update(updated) map
                      (_ => Redirect(routes.UserController.index()))
                  } else {
                    val form = profileForm.fill(profile)
                    val updatedForm = Form(
                      form.mapping,
                      form.data,
                      Seq(
                        new FormError(
                          "isAdmin",
                          Messages("admin.error.noAdmins"),
                        ),
                      ),
                      form.value,
                    )
                    generateResponse(
                      request.value,
                      id,
                      BadRequest,
                      updatedForm,
                      passwordForm,
                    )
                  }
              } yield result,
          ),
      )

  def updatePassword(id: User.Id) =
    silhouette
      .SecuredAction(WithAdmin)
      .andThen(UserAction(db, id))
      .async { implicit c =>
        passwordForm
          .bindFromRequest()(c)
          .fold(
            formWithErrors =>
              generateResponse(
                c.value,
                id,
                BadRequest,
                filledProfileForm(c.value),
                formWithErrors,
              ),
            password =>
              db.run(
                UserDataDBIO.update(
                  c.value.data.loginInfo,
                  passwordHasherRegistry.current hash password,
                ),
              )
                .map(_ =>
                  Redirect(routes.UserController.index()) flashing
                    StatusType.Success.entryName ->
                    Messages("users.password.updated", c.value.data.email),
                ),
          )
      }

  def delete(id: User.Id) =
    silhouette
      .SecuredAction(WithAdmin)
      .andThen(UserAction(db, id))
      .async(implicit request =>
        for {
          notAlone <- db run UserDataDBIO.existActiveAdminsExcept(id)
          result <-
            if (notAlone)
              db run UserDBIO.deleteById(id) map
                (_ => Redirect(routes.UserController.index()))
            else
              Future successful
                (Redirect(routes.UserController.index()) flashing
                  StatusType.Danger.entryName -> Messages(
                    "users.cannotDeleteLastAdmin",
                  ))
        } yield result,
      )

}
