package controllers.auth

import com.mohiva.play.silhouette.api.Authenticator.Implicits._
import com.mohiva.play.silhouette.api._
import com.mohiva.play.silhouette.api.exceptions.ProviderException
import com.mohiva.play.silhouette.api.util.{Clock, Credentials}
import com.mohiva.play.silhouette.impl.exceptions.IdentityNotFoundException
import com.mohiva.play.silhouette.impl.providers._
import data.db.UserDataDBIO
import forms.SignInForm
import net.ceedubs.ficus.Ficus._
import play.api.Configuration
import play.api.i18n.{I18nSupport, Messages}
import play.api.mvc.{AnyContent, BaseController, ControllerComponents, Request}
import silhouetteIntegration.{DefaultEnv, UserIdentityService}
import slick.jdbc.JdbcBackend
import utils.FrontComponents

import scala.concurrent.Future
import scala.concurrent.duration._

final class SignInController(
  val controllerComponents: ControllerComponents,
  db: JdbcBackend#DatabaseDef,
  silhouette: Silhouette[DefaultEnv],
  userService: UserIdentityService,
  credentialsProvider: CredentialsProvider,
  configuration: Configuration,
  clock: Clock,
)(implicit
  frontComponents: FrontComponents,
) extends BaseController
    with I18nSupport {

  implicit lazy val executionContext = controllerComponents.executionContext

  /**
   * Views the `Sign In` page.
   *
   * @return
   *   The result to display.
   */
  def view =
    silhouette.UnsecuredAction.async { implicit request: Request[AnyContent] =>
      Future.successful(Ok(views.html.auth.signIn(SignInForm.form)))
    }

  /**
   * Handles the submitted form.
   *
   * @return
   *   The result to display.
   */
  def submit =
    silhouette.UnsecuredAction.async { implicit request: Request[AnyContent] =>
      SignInForm.form
        .bindFromRequest().fold(
          form => Future.successful(BadRequest(views.html.auth.signIn(form))),
          data => {
            val credentials = Credentials(data.email, data.password)
            credentialsProvider
              .authenticate(credentials)
              .flatMap { loginInfo =>
                val result =
                  Redirect(controllers.routes.TopController.index())
                db.run(UserDataDBIO.findOne(loginInfo)).flatMap {
                  case Some(user) if !user.activated =>
                    Future.successful(
                      Ok(views.html.auth.activateAccount(data.email)),
                    )
                  case Some(user) =>
                    val c = configuration.underlying
                    silhouette.env.authenticatorService
                      .create(loginInfo)
                      .map {
                        case authenticator if data.rememberMe =>
                          authenticator.copy(
                            expirationDateTime = clock.now + c
                              .as[FiniteDuration](
                                "silhouette.authenticator.rememberMe.authenticatorExpiry",
                              ),
                            idleTimeout = c.getAs[FiniteDuration](
                              "silhouette.authenticator.rememberMe.authenticatorIdleTimeout",
                            ),
                            cookieMaxAge = c.getAs[FiniteDuration](
                              "silhouette.authenticator.rememberMe.cookieMaxAge",
                            ),
                          )
                        case authenticator => authenticator
                      }
                      .flatMap { authenticator =>
                        silhouette.env.eventBus
                          .publish(LoginEvent(user, request))
                        silhouette.env.authenticatorService
                          .init(authenticator)
                          .flatMap { v =>
                            silhouette.env.authenticatorService.embed(v, result)
                          }
                      }
                  case None =>
                    Future.failed(
                      new IdentityNotFoundException("Couldn't find user"),
                    )
                }
              }
              .recover { case _: ProviderException =>
                Redirect(routes.SignInController.view())
                  .flashing("danger" -> Messages("invalid.credentials"))
              }
          },
        )
    }
}
