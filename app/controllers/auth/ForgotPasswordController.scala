package controllers.auth

import com.mohiva.play.silhouette.api._
import com.mohiva.play.silhouette.impl.providers.CredentialsProvider
import data.db.{AuthTokenDBIO, UserDBIO, UserDataDBIO}
import forms.ForgotPasswordForm
import play.api.i18n.{I18nSupport, Messages}
import play.api.libs.mailer.{Email, MailerClient}
import play.api.mvc.{BaseController, ControllerComponents}
import silhouetteIntegration.{DefaultEnv, UserIdentityService}
import slick.jdbc.JdbcBackend
import utils.FrontComponents

import scala.concurrent.Future

final class ForgotPasswordController(
  val controllerComponents: ControllerComponents,
  db: JdbcBackend#DatabaseDef,
  silhouette: Silhouette[DefaultEnv],
  userService: UserIdentityService,
  mailerClient: MailerClient,
)(implicit
  frontComponents: FrontComponents,
) extends BaseController
    with I18nSupport {

  implicit lazy val executionContext = controllerComponents.executionContext

  /**
   * Views the `Forgot Password` page.
   *
   * @return
   *   The result to display.
   */
  def view =
    silhouette.UnsecuredAction.async { implicit request =>
      Future.successful(
        Ok(views.html.auth.forgotPassword(ForgotPasswordForm.form)),
      )
    }

  /**
   * Sends an email with password reset instructions.
   *
   * It sends an email to the given address if it exists in the database.
   * Otherwise we do not show the user a notice for not existing email addresses
   * to prevent the leak of existing email addresses.
   *
   * @return
   *   The result to display.
   */
  def submit =
    silhouette.UnsecuredAction.async { implicit request =>
      ForgotPasswordForm.form
        .bindFromRequest().fold(
          form =>
            Future.successful(BadRequest(views.html.auth.forgotPassword(form))),
          email => {
            val loginInfo = LoginInfo(CredentialsProvider.ID, email)
            val result = Redirect(routes.SignInController.view())
              .flashing("info" -> Messages("reset.email.sent"))
            db.run(UserDataDBIO.findOne(loginInfo)).flatMap {
              case Some(user) =>
                db.run(AuthTokenDBIO save user.freshToken()).map { authToken =>
                  val url = routes.ResetPasswordController
                    .view(authToken.id)
                    .absoluteURL()

                  mailerClient.send(
                    Email(
                      subject = Messages("email.reset.password.subject"),
                      from = Messages("email.from"),
                      to = Seq(email),
                      bodyText = Some(
                        views.txt.auth.emails.resetPassword(user, url).body,
                      ),
                      bodyHtml = Some(
                        views.html.auth.emails.resetPassword(user, url).body,
                      ),
                    ),
                  )
                  result
                }
              case None => Future.successful(result)
            }
          },
        )
    }
}
