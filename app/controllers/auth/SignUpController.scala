package controllers.auth

import com.mohiva.play.silhouette.api._
import com.mohiva.play.silhouette.api.repositories.AuthInfoRepository
import com.mohiva.play.silhouette.api.util.PasswordHasherRegistry
import com.mohiva.play.silhouette.impl.providers._
import data.db.{AuthTokenDBIO, UserDBIO, UserDataDBIO}
import forms.SignUpForm
import models.UserData
import play.api.http.HttpErrorHandler
import play.api.i18n.{I18nSupport, Messages}
import play.api.libs.mailer.{Email, MailerClient}
import play.api.mvc.{BaseController, ControllerComponents}
import silhouetteIntegration.{DefaultEnv, UserIdentityService}
import slick.jdbc.JdbcBackend
import utils.FrontComponents

import scala.concurrent.Future

final class SignUpController(
  val controllerComponents: ControllerComponents,
  db: JdbcBackend#DatabaseDef,
  silhouette: Silhouette[DefaultEnv],
  userService: UserIdentityService,
  authInfoRepository: AuthInfoRepository,
  passwordHasherRegistry: PasswordHasherRegistry,
  mailerClient: MailerClient,
)(implicit
  frontComponents: FrontComponents,
  httpErrorHandler: HttpErrorHandler,
) extends BaseController
    with I18nSupport {

  implicit lazy val executionContext = controllerComponents.executionContext

  /**
   * Views the `Sign Up` page.
   *
   * @return
   *   The result to display.
   */
  def view =
    silhouette.UnsecuredAction.async(implicit request =>
      for {
        notAlone <- db run UserDataDBIO.existActiveAdmins
        result <-
          if (notAlone)
            httpErrorHandler
              .onClientError(request, NOT_FOUND, "sign up is disabled")
          else
            Future successful Ok(
              views.html.auth.signUp(SignUpForm.form, !notAlone),
            )
      } yield result,
    )

  /**
   * Handles the submitted form.
   *
   * @return
   *   The result to display.
   */
  def submit =
    silhouette.UnsecuredAction.async(implicit request =>
      for {
        notAlone <- db run UserDataDBIO.existActiveAdmins
        result <-
          if (notAlone)
            httpErrorHandler
              .onClientError(request, NOT_FOUND, "sign up is disabled")
          else
            SignUpForm.form
              .bindFromRequest().fold(
                form =>
                  Future successful
                    BadRequest(views.html.auth.signUp(form, !notAlone)),
                data => {
                  val result = Redirect(routes.SignUpController.view()) flashing
                    "info" -> Messages("sign.up.email.sent", data.email)
                  val loginInfo = LoginInfo(CredentialsProvider.ID, data.email)
                  db.run(UserDBIO.findOne(loginInfo)).flatMap {
                    case Some(user) =>
                      val url = routes.SignInController.view().absoluteURL()
                      mailerClient.send(
                        Email(
                          subject = Messages("email.already.signed.up.subject"),
                          from = Messages("email.from"),
                          to = Seq(data.email),
                          bodyText = Some(
                            views.txt.auth.emails
                              .alreadySignedUp(user, url).body,
                          ),
                          bodyHtml = Some(
                            views.html.auth.emails
                              .alreadySignedUp(user, url)
                              .body,
                          ),
                        ),
                      )
                      Future.successful(result)
                    case None =>
                      val authInfo =
                        passwordHasherRegistry.current.hash(data.password)
                      val userData = UserData.withLoginInfo(
                        loginInfo = loginInfo,
                        firstName = Some(data.firstName),
                        lastName = Some(data.lastName),
                        fullName = Some(data.firstName + " " + data.lastName),
                        email = data.email,
                        activated = false,
                        hasher = authInfo.hasher,
                        password = authInfo.password,
                        salt = authInfo.salt,
                        isAdmin = !notAlone && data.isAdmin,
                      )
                      for {
                        saved <- db.run(UserDataDBIO.save(userData))
                        _     <- authInfoRepository.add(loginInfo, authInfo)
                        authToken <-
                          db.run(AuthTokenDBIO save saved.freshToken())
                      } yield {
                        val url = routes.ActivateAccountController
                          .activate(authToken.id)
                          .absoluteURL()
                        mailerClient.send(
                          Email(
                            subject = Messages("email.sign.up.subject"),
                            from = Messages("email.from"),
                            to = Seq(data.email),
                            bodyText = Some(
                              views.txt.auth.emails.signUp(saved, url).body,
                            ),
                            bodyHtml = Some(
                              views.html.auth.emails.signUp(saved, url).body,
                            ),
                          ),
                        )
                        silhouette.env.eventBus
                          .publish(SignUpEvent(saved, request))
                        result
                      }
                  }
                },
              )
      } yield result,
    )
}
