package config

import pureconfig.generic.semiauto._

final case class HostingConfig(host: String)

object HostingConfig {
  implicit val hostingConfigReader = deriveReader[HostingConfig]
}
