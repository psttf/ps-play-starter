package infrastructure
import java.time.ZoneOffset
import java.util.{Locale, TimeZone}

import play.api.Logger

trait TimeZoneComponents {

  private lazy val logger = Logger(getClass)

  val timeZone = TimeZone.getTimeZone(ZoneOffset.UTC)
  TimeZone.setDefault(timeZone)
  logger info s"TimeZone was set to $timeZone"

  val locale = Locale.US
  Locale.setDefault(locale)
  logger info s"Locale was set to $locale"

}
