package silhouetteIntegration

import com.mohiva.play.silhouette.api.repositories.AuthenticatorRepository
import com.mohiva.play.silhouette.impl.authenticators.CookieAuthenticator
import data.db.CookieAuthenticatorDBIO
import slick.basic.DatabaseConfig
import slick.jdbc.JdbcBackend

import scala.concurrent.Future

class CookieAuthenticatorRepository(db: JdbcBackend#DatabaseDef)
    extends AuthenticatorRepository[CookieAuthenticator] {

  override def find(
    id: String,
  ): Future[Option[CookieAuthenticator]] =
    db.run(CookieAuthenticatorDBIO.find(id))

  override def add(
    authenticator: CookieAuthenticator,
  ): Future[CookieAuthenticator] =
    db.run(CookieAuthenticatorDBIO.add(authenticator))

  override def update(
    authenticator: CookieAuthenticator,
  ): Future[CookieAuthenticator] =
    db.run(CookieAuthenticatorDBIO.update(authenticator))

  override def remove(
    id: String,
  ): Future[Unit] =
    db.run(CookieAuthenticatorDBIO.remove(id))

}
