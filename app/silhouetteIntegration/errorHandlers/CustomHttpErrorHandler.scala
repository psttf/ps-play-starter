package silhouetteIntegration.errorHandlers

import com.rollbar.Rollbar
import com.rollbar.payload.data.Request
import play.api._
import play.api.http.DefaultHttpErrorHandler
import play.api.i18n.{I18nSupport, MessagesApi}
import play.api.mvc.Results._
import play.api.mvc.{RequestHeader, Result, Results}
import play.api.routing.Router
import play.core.SourceMapper
import utils.FrontComponents

import scala.jdk.CollectionConverters._
import scala.concurrent.{ExecutionContext, Future}

class CustomHttpErrorHandler(
  environment: Environment,
  config: Configuration,
  rollbar: Option[Rollbar],
  sourceMapper: Option[SourceMapper],
  router: => Option[Router],
  val messagesApi: MessagesApi,
)(implicit
  frontComponents: FrontComponents,
  ex: ExecutionContext,
) extends DefaultHttpErrorHandler(environment, config, sourceMapper, router)
    with I18nSupport {

  lazy val showDevErrors = environment.mode != Mode.Prod

  override protected def logServerError(
    request: RequestHeader,
    usefulException: UsefulException,
  ) =
    rollbar.fold(super.logServerError(request, usefulException))(
      _.request(
        new Request()
          .url(request.uri)
          .method(request.method)
          .headers(request.headers.toSimpleMap.asJava)
          .setGet(
            request.queryString
              .flatMap(pair => pair._2.headOption.map(pair._1 -> _))
              .asJava,
          )
          .queryString(request.rawQueryString: String),
      ).log(usefulException),
    )

  /**
   * Invoked when a client makes a bad request.
   *
   * @param request
   *   The request that was bad.
   * @param message
   *   The error message.
   */
  override protected def onBadRequest(
    request: RequestHeader,
    message: String,
  ): Future[Result] = {
    implicit val impRequest = request
    implicit val flash = request.flash
    Future successful
      BadRequest(
        views.html.errors.badRequest(request.method, request.uri, message),
      )
  }

  /**
   * Invoked when a client makes a request that was forbidden.
   *
   * @param request
   *   The forbidden request.
   * @param message
   *   The error message.
   */
  override protected def onForbidden(
    request: RequestHeader,
    message: String,
  ): Future[Result] = {
    implicit val impRequest = request
    implicit val flash = request.flash
    Future successful
      Forbidden(views.html.errors.unauthorized())
  }

  /**
   * Invoked when a handler or resource is not found.
   *
   * @param request
   *   The request that no handler was found to handle.
   * @param message
   *   A message.
   */
  override protected def onNotFound(
    request: RequestHeader,
    message: String,
  ): Future[Result] = {
    implicit val impRequest = request
    implicit val flash = request.flash
    Future successful
      NotFound(
        if (showDevErrors)
          views.html.defaultpages
            .devNotFound(request.method, request.uri, router)
        else
          views.html.errors.notFound(request.method, request.uri, message),
      )
  }

  /**
   * Invoked when a client error occurs, that is, an error in the 4xx series,
   * which is not handled by any of the other methods in this class already.
   *
   * @param request
   *   The request that caused the client error.
   * @param statusCode
   *   The error status code. Must be greater or equal to 400, and less than
   *   500.
   * @param message
   *   The error message.
   */
  override protected def onOtherClientError(
    request: RequestHeader,
    statusCode: Int,
    message: String,
  ): Future[Result] = {
    implicit val impRequest = request
    implicit val flash = request.flash
    Future successful
      Results.Status(statusCode)(
        views.html.errors.badRequest(request.method, request.uri, message),
      )
  }

  /**
   * Invoked in prod mode when a server error occurs.
   *
   * Override this rather than [[onServerError]] if you don't want to change
   * Play's debug output when logging errors in dev mode.
   *
   * @param request
   *   The request that triggered the error.
   * @param exception
   *   The exception.
   */
  override protected def onProdServerError(
    request: RequestHeader,
    exception: UsefulException,
  ): Future[Result] = {
    implicit val impRequest = request
    implicit val flash = request.flash
    Future successful
      InternalServerError(views.html.errors.usefulException(exception))
  }

}
