package silhouetteIntegration

import com.mohiva.play.silhouette.api.Env
import com.mohiva.play.silhouette.api.actions.SecuredRequest
import com.mohiva.play.silhouette.impl.authenticators.CookieAuthenticator
import models.User

/**
 * The default env.
 */
trait DefaultEnv extends Env {
  type I = User
  type A = CookieAuthenticator
}

object DefaultEnv {
  type SecuredRequestAux[X] = SecuredRequest[DefaultEnv, X]
}
