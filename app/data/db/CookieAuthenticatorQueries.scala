package data.db

import data.db.SlickPgProfile.api._
import data.db.SlickPgProfile.mapping._

object CookieAuthenticatorQueries {

  val table = TableQuery(new CookieAuthenticatorTable(_))

  def schema = table.schema

  val byId = Compiled((id: Rep[String]) => table.filter(_.id === id))

}
