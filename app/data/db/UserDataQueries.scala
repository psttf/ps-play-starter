package data.db

import models.{User, UserData}
import data.db.SlickPgProfile.api._
import data.db.SlickPgProfile.mapping._
import slick.lifted.ProvenShape

final class UserDataTable(tag: Tag) extends Table[UserData](tag, "User") {

  def id = column[User.Id]("id", O.PrimaryKey, O.AutoInc)
  def providerID = column[String]("providerID")
  def providerKey = column[String]("providerKey")
  def firstName = column[Option[String]]("firstName")
  def lastName = column[Option[String]]("lastName")
  def fullName = column[Option[String]]("fullName")
  def email = column[String]("email")
  def activated = column[Boolean]("activated")
  def hasher = column[String]("hasher")
  def password = column[String]("password")
  def salt = column[Option[String]]("salt")
  def isAdmin = column[Boolean]("isAdmin")

  override def * : ProvenShape[UserData] =
    (
      id,
      providerID,
      providerKey,
      firstName,
      lastName,
      fullName,
      email,
      activated,
      hasher,
      password,
      salt,
      isAdmin,
    ) <>
      ((UserData.apply _).tupled, UserData.unapply)

}

object UserDataQueries {

  val table = TableQuery(new UserDataTable(_))

  val findOne = Compiled((providerID: Rep[String], providerKey: Rep[String]) =>
    table
      .filter(_.providerID === providerID)
      .filter(_.providerKey === providerKey),
  )

  val byUserId = Compiled((id: Rep[User.Id]) => table.filter(_.id === id))

  val byEmail =
    Compiled((email: Rep[String]) => table.filter(_.email === email))

  val existActiveAdmins = Compiled(
    table
      .filter(_.isAdmin)
      .filter(_.activated)
      .exists,
  )

  val existActiveAdminsExcept = Compiled((id: Rep[User.Id]) =>
    table
      .filter(_.isAdmin)
      .filter(_.activated)
      .filter(_.id =!= id)
      .exists,
  )

  val passwordInfoByLoginInfo =
    Compiled((providerID: Rep[String], providerKey: Rep[String]) =>
      findOne
        .extract(providerID, providerKey)
        .map(user => (user.hasher, user.password, user.salt)),
    )

  def schema = table.schema

}
