package data.db

import java.util.UUID

import models.{AuthToken, User}
import org.joda.time.DateTime
import slick.dbio.DBIO
import data.db.SlickPgProfile.api._
import data.db.SlickPgProfile.mapping._

import scala.concurrent.ExecutionContext.Implicits.global

object AuthTokenDBIO {

  def lookup(key: UUID): DBIO[Option[AuthToken]] =
    for {
      queried <- AuthTokenQueries.byId(key).result
      result <-
        if (queried.size > 1)
          DBIO failed new Exception(s"Many tokens with id $key")
        else
          DBIO successful queried.headOption
    } yield result

  def findExpired(dateTime: DateTime): DBIO[Seq[AuthToken]] =
    AuthTokenQueries.expired(dateTime).result

  def findByUserId(userId: User.Id): DBIO[Option[AuthToken]] =
    AuthTokenQueries.byUserId(userId).result.headOption

  def save(entity: AuthToken): DBIO[AuthToken] =
    for { _ <- AuthTokenQueries.table += entity } yield entity

  def remove(id: UUID): DBIO[Unit] =
    AuthTokenQueries.byId(id).delete map (_ => ())

}
