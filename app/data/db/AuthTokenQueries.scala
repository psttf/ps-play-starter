package data.db

import java.util.UUID

import com.github.tototoshi.slick.PostgresJodaSupport._
import data.db.SlickPgProfile.api._
import models.{AuthToken, User}
import org.joda.time.DateTime
import slick.lifted.ProvenShape

final class AuthTokenTable(tag: Tag)
    extends Table[AuthToken](tag, "AuthToken") {
  def id = column[UUID]("id", O.PrimaryKey)
  def userId = column[User.Id]("userId")
  def expiry = column[DateTime]("expiry")

  override def * : ProvenShape[AuthToken] =
    (
      id,
      userId,
      expiry,
    ) <>
      ((AuthToken.apply _).tupled, AuthToken.unapply)

}

object AuthTokenQueries {

  val table = TableQuery(new AuthTokenTable(_))

  def schema = table.schema

  val byId = Compiled((id: Rep[UUID]) => table.filter(_.id === id))

  val byUserId =
    Compiled((userId: Rep[User.Id]) => table.filter(_.userId === userId))

  val expired =
    Compiled((expiry: Rep[DateTime]) => table.filter(_.expiry < expiry))

}
