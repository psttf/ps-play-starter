package forms

import play.api.data.Form
import play.api.data.Forms._

object GuestForm {

  val form = Form(
    mapping(
      "password" -> nonEmptyText,
      SignUpForm.agreedToTerms,
    )(Data.apply)(Data.unapply),
  )

  case class Data(
    password: String,
    agreedToTerms: Boolean,
  )

}
