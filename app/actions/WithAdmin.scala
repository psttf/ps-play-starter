package actions

import com.mohiva.play.silhouette.api.Authorization
import models.User
import play.api.mvc._
import silhouetteIntegration.DefaultEnv

import scala.concurrent.Future

object WithAdmin extends Authorization[User, DefaultEnv#A] {
  override def isAuthorized[B](
    user: User,
    authenticator: DefaultEnv#A,
  )(implicit request: Request[B]): Future[Boolean] =
    Future successful user.data.isAdmin
}
