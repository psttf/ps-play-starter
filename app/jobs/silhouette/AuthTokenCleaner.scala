package jobs.silhouette

import akka.actor._
import com.mohiva.play.silhouette.api.util.Clock
import com.rollbar.Rollbar
import data.db.AuthTokenDBIO
import jobs.IsIgnoredRejectedExecutionException
import jobs.silhouette.AuthTokenCleaner.Clean
import org.joda.time.DateTimeZone
import slick.jdbc.JdbcBackend

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class AuthTokenCleaner(
  db: JdbcBackend#DatabaseDef,
  clock: Clock,
  rollbar: Option[Rollbar],
) extends Actor
    with ActorLogging {

  def receive: Receive = { case Clean =>
    val start = clock.now.getMillis
    val msg = new StringBuffer("\n")
    msg.append("=================================\n")
    msg.append("Start to cleanup auth tokens\n")
    msg.append("=================================\n")
    clean.map { deleted =>
      val seconds = (clock.now.getMillis - start) / 1000
      msg
        .append(
          "Total of %s auth tokens(s) were deleted in %s seconds"
            .format(deleted.length, seconds),
        )
        .append("\n")
      msg.append("=================================\n")

      msg.append("=================================\n")
      log.info(msg.toString)
    }.recover {
      case IsIgnoredRejectedExecutionException(recognized) =>
        Future successful recognized.ignore()
      case e =>
        msg.append(
          "Couldn't cleanup auth tokens because of unexpected error\n",
        )
        msg.append("=================================\n")
        rollbar.fold(log.error(msg.toString, e))(_ log e)
    }
  }

  def clean =
    db run
      AuthTokenDBIO.findExpired(clock.now.withZone(DateTimeZone.UTC)) flatMap (
        tokens =>
          Future.sequence(
            tokens map (token =>
              db.run(AuthTokenDBIO.remove(token.id)).map(_ => token),
            ),
          ),
      )

}

object AuthTokenCleaner {
  case object Clean
}

case class AuthTokenCleanerWrapper(underlying: ActorRef) extends AnyVal
