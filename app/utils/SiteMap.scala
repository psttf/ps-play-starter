package utils

import play.api.Logger
import play.api.i18n.Messages
import play.api.mvc.Call
import play.twirl.api.BaseScalaTemplate

sealed trait NavItem {

  def id: String

  def children: List[NavItem]
  def descendants: List[NavItem] =
    children.flatMap(child => child :: child.descendants)
  def active(nav: Option[NavItem]) =
    nav.exists(page =>
      this == page ||
        (this != SiteMap.root && descendants.contains(page)),
    )
  def pathTo(nav: NavItem): List[NavItem] =
    if (this == nav) List(this)
    else
      children.map(_.pathTo(nav)).find(_.nonEmpty).map(this :: _) getOrElse Nil

  def messageId: Option[String]
  def navMessage()(implicit messages: Messages) =
    messages(messageId getOrElse s"nav.$id")

  def showChildren: Boolean
  lazy val shownChildren = if (showChildren) children else Nil

}

final case class NavSection(
  id: String,
  children: List[NavItem] = Nil,
  messageId: Option[String] = None,
  showChildren: Boolean = true,
) extends NavItem

final case class Page(
  id: String,
  children: List[NavItem] = Nil,
  call: Call,
  messageId: Option[String] = None,
  showChildren: Boolean = true,
) extends NavItem

object SiteMap {

  private lazy val logger = Logger(getClass)

  lazy val users = Page(
    "users",
    call = controllers.routes.UserController.index(),
  )

  lazy val root = Page(
    "top.index",
    call = controllers.routes.TopController.index(),
  )

  def apply(id: String): Option[NavItem] =
    (root :: root.descendants).find(_.id == id) orElse {
      logger warn s"no SiteMap page $id"
      None
    }

  def apply(view: BaseScalaTemplate[_, _]): Option[NavItem] =
    apply(id(view))

  private def id(view: BaseScalaTemplate[_, _]) = {
    val name = view.getClass.toString
    val r = "class ?views\\.html\\.?(.*)\\$".r
    name match {
      case r(inner) => inner
      case _        => name
    }
  }

  def nav_id(view: BaseScalaTemplate[_, _]) =
    s"nav.${id(view)}"

}
