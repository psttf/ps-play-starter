package utils

import play.api.mvc.{Request, WrappedRequest}

import scala.language.higherKinds

class TypedWrappedRequest[R[X] <: Request[X], A](
  val request: R[A],
) extends WrappedRequest[A](request)
