@import config.RollbarConfig
@import org.webjars.play.WebJarsUtil
@import play.twirl.api.StringInterpolation

@(maybeConfig: Option[RollbarConfig])(implicit webJarsUtil: WebJarsUtil)

var _rollbarConfig = {
  rollbarJsUrl:
    @webJarsUtil.locate("dist/rollbar.min.js").url.fold(
      _ => js"null",
      rollbarJsUrl => js"'$rollbarJsUrl'"
    ),
  @maybeConfig.fold{
    enabled: false
  }{ config =>
    enabled: true,
    accessToken: "@config.clientAccessToken",
    captureUncaught: true,
    captureUnhandledRejections: true,
    payload: { environment: "@config.environment"}
  }
};
